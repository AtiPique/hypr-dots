# Hypr dots 

![preview](image.webp)

#### Needed softwares :

```sudo pacman -S hyprland waybar swww dunst hyprshot alacritty ```

#### Optional softwares : 

```sudo pacman -S firefox bat exa```
```yay -S vscodium-bin```

#### Rice install :

```cp -r ~/hypr-dots/.config/* ~/.config/```
```sudo cp -r ~/hypr-dots/JetBrainsMono /usr/share/fonts/```

#### Theme switcher :

##### Dependencies :
[swww](https://github.com/LGFae/swww) wallpaper daemon

[pywal](https://github.com/dylanaraps/pywal) helps getting colors from the image provided.

[pyyaml](https://pypi.org/project/PyYAML/) python library used in the script alacritty.py to turn .yml file to .toml to be used with alacritty

##### usage : 
`python3 main.py <path-to-image.jpg>` change temporaly the theme for alacritty, reload waybar and changes the wallpaper

`python3 main.py -nw <path-to-image.jpg>` change temporaly the theme for alacritty, no waybar reload and changes the wallpaper

`python3 alacritty <path-to-alacritty-color-file>` convert .yml color file generated by pywal to .toml for alacritty 

`python3 alacritty <path-to-hypr-color-file>` convert .yml color file generated by pywal to .conf to change hyprland border colors.

#### Licence 

![gplv3](./gplv3.png)