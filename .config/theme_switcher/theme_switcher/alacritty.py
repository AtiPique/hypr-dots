"""
convert .yml color scheme to .toml for alacritty

args : path and name for the alacritty colors file (ex : /home/user/alacritty/colors.toml)
"""

import sys
import getpass
import yaml

color = ['black', 'magenta', 'green', 'yellow', 'blue', 'red', 'cyan', 'white']

user = getpass.getuser()

with open(f"/home/{user}/.cache/wal/colors.yml") as y:
    colors = yaml.safe_load(y)


def custom(path):
    f = open(f"{path}", "w")

    # primary colors

    f.write("[colors.primary]\n")
    background = colors['special']['background']
    f.write(f'\tbackground = "{background}"\n')
    foreground = colors['special']['foreground']
    f.write(f'\tforeground = "{foreground}"\n\n')


    # normal colors

    f.write("[colors.normal]\n")
    for i in range(len(color)):
        c = colors['colors'][f'color{i}']
        f.write(f'\t{color[i]} = "{c}"\n')

    # bright colors

    f.write("\n[colors.bright]\n")
    for i in range(len(color)):
        c = colors['colors'][f'color{i+8}']
        f.write(f'\t{color[i]} = "{c}"\n')

if sys.argv[1]:
    custom(sys.argv[1])
