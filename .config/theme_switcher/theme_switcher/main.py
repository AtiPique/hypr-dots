import sys, os
import alacritty, hypr

if "-nw" in sys.argv: # no reload waybar
    img = sys.argv[2]
    cmd = f"wal -i {img} -e && swww img {img}"

    os.system(cmd)
    
    os.system(f"cp -r {img} ~/.config/wallpaper.jpg")
    os.system("hyprctl reload")

    print(f"Changed theme for the image {img}")
    exit()

else:
    img = sys.argv[1]
    cmd = f"wal -i {img} -e && swww img {img} && sh ~/.config/waybar/launch.sh"

    os.system(cmd)
    os.system("sh ~/.config/waybar/launch.sh")
    os.system(f"cp -r {img} ~/.config/wallpaper.jpg")
    os.system("hyprctl reload")

    print(f"Changed theme for the image {img}")
    exit() 
