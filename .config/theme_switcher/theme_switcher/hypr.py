"""
change hyprland border colors based on the background and foreground color
provided by pywal in color.yml files

args : hyprland colors.conf path. (ex : /home/user/.config/hypr/colors.conf)
"""

import sys
import getpass
import yaml

user = getpass.getuser()

with open(f"/home/{user}/.cache/wal/colors.yml") as y:
    colors = yaml.safe_load(y)

def custom(path):
    f = open(f"{path}", "w")

    f.write("[colors.primary]\n")

    background = colors['special']['background']
    background = background.replace("#", "")
    f.write(f'\t$background = {background}\n')

    foreground = colors['special']['foreground']
    foreground = foreground.replace("#", "")
    f.write(f'\t$foreground = {foreground}\n\n')

    print("Hypr border colors set.")

if sys.argv[1]:
    custom(sys.argv[1])
